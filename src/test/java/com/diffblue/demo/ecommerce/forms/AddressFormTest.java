package com.diffblue.demo.ecommerce.forms;

import com.diffblue.demo.ecommerce.forms.AddressForm;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;

import java.lang.reflect.Method;

public class AddressFormTest {

  @Rule public ExpectedException thrown = ExpectedException.none();
  @Rule public Timeout globalTimeout = new Timeout(10000);

  /* testedClasses: AddressForm */
  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers the entire method.
   */

  @Test
  public void constructorOutputVoid() {

    // Act, creating object to test constructor
    final AddressForm objectUnderTest = new AddressForm();

    // Method returns void, testing that no exception is thrown
  }

}
