package com.diffblue.demo.ecommerce.models;

import com.diffblue.demo.ecommerce.models.Collection;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;

import java.lang.reflect.Method;

public class CollectionTest {

  @Rule public ExpectedException thrown = ExpectedException.none();
  @Rule public Timeout globalTimeout = new Timeout(10000);

  /* testedClasses: Collection */
  /*
   * Test generated by Diffblue Deeptest.
   * This test case covers the entire method.
   */

  @Test
  public void constructorOutputVoid() {

    // Act, creating object to test constructor
    final Collection objectUnderTest = new Collection();

    // Method returns void, testing that no exception is thrown
  }
  /*
 * Test generated by Diffblue Deeptest.
 * This test case covers the entire method.
 */

      @Test public void getIdOutputZero() {

    // Arrange
    final Collection objectUnderTest = new Collection();

    // Act
    final int retval = objectUnderTest.getId();

    // Assert result
    Assert.assertEquals(0, retval);
  }
  /*
 * Test generated by Diffblue Deeptest.
 * This test case covers the entire method.
 */

      @Test public void getNameOutputNull() {

    // Arrange
    final Collection objectUnderTest = new Collection();

    // Act
    final String retval = objectUnderTest.getName();

    // Assert result
    Assert.assertNull(retval);
  }
  /*
 * Test generated by Diffblue Deeptest.
 * This test case covers the entire method.
 */

      @Test public void setIdInputZeroOutputVoid() {

    // Arrange
    final Collection objectUnderTest = new Collection();
    final int newId = 0;

    // Act
    objectUnderTest.setId(newId);

    // Method returns void, testing that no exception is thrown
  }
  /*
 * Test generated by Diffblue Deeptest.
 * This test case covers:
 *  - conditional line 66 branch to line 69
 */

      @Test public void setNameInputNotNullOutputVoid() {

    // Arrange
    final Collection objectUnderTest = new Collection();
    final String newName = "";

    // Act
    objectUnderTest.setName(newName);

    // Method returns void, testing that no exception is thrown
  }
  /*
 * Test generated by Diffblue Deeptest.
 * This test case covers:
 *  - conditional line 66 branch to line 67
 */

      @Test public void setNameInputNotNullOutputVoid2() {

    // Arrange
    final Collection objectUnderTest = new Collection();
    final String newName = " ";

    // Act
    objectUnderTest.setName(newName);

    // Method returns void, testing that no exception is thrown
  }
}
