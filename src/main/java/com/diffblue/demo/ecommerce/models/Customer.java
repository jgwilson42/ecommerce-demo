package com.diffblue.demo.ecommerce.models;

public class Customer {

  private String name; // Full name of the customer
  private String address; // Billing address
  private String phoneNumber; // Contact phone number

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * Check that the phoneNumber has the correct number of digits.
   * @param phoneNumber input is the phone number
   * @throws IllegalAccessException if the number of the digits is incorrect
   */
  public void setPhoneNumber(String phoneNumber) throws IllegalAccessException {
    // Area code is 5 digits and number is 6 digits
    if ( phoneNumber.length() < 11 ) {
      throw new IllegalAccessException();
    }
    this.phoneNumber = phoneNumber;
  }


}
