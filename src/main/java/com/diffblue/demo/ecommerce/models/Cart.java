package com.diffblue.demo.ecommerce.models;

// Copyright 2016-2018 Diffblue Limited. All rights reserved.

import java.util.HashMap;
import java.util.Map;

public class Cart {

  private Map<Product, Integer> cartItems;
  private double subtotal;
  private double shipping;
  private double tax;
  private double taxRate;
  private double total;

  /**
   * Constructor.
   */
  public Cart() {
    cartItems = new HashMap<>();
    subtotal = 0;
    shipping = 4.50;
    tax = 0;
    taxRate = 0.20;
    total = 0;
  }

  /**
   * Add product to card.
   * @param product the product to add to the cart
   */

  public void addProduct(Product product) {
    // TODO: Implement this
  }

  public Map<Product, Integer> getProducts() {
    return cartItems;
  }

  /**
   * Get cart subtotal.
   * @return products subtotal
   */

  public double getTax() {
    return tax;
  }

  /**
   * Get cart subtotal.
   * @return products subtotal
   */

  public double getSubtotal() {
    return subtotal;
  }

  public void setSubtotal(double subtotal) {
    this.subtotal = subtotal;
  }

  /**
   * Update product quantity in the cart.
   * @param product product to be update, newQty new quantity
   */
  public void updateProductQuantity(Product product, int newQty, String size) {
    // TODO: Implement this
  }

  /**
  * Returns the first product in the cart with a quantity < 0.
  * If cart contains no such products, null is returned.
  */
  public Product checkInvalid() {
    // TODO: Implement this
    return null;
  }
  
  public double getShipping() {
    return shipping;
  }

  public void setTotal(double tax, double shipping) {
    this.total = shipping + tax;
  }

  public double getTotal() {
    return total + subtotal;
  }

}
